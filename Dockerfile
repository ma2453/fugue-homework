FROM nginx:latest

RUN apt-get update && apt-get install -y vim
COPY default.conf /etc/nginx/conf.d/
COPY static/* /usr/share/nginx/html/

EXPOSE 8080
